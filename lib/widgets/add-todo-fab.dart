import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myapp/cubits/todo-cubit.dart';
import 'package:myapp/models/todo.dart';

class AddTodoFab extends StatelessWidget {
  const AddTodoFab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      child: const Icon(Icons.add),
      onPressed: () {
        int id = Random().nextInt(200);
        TextEditingController textController = TextEditingController();
        showDialog(
            context: context,
            builder: ((context) => Dialog(
                  child: IntrinsicHeight(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        // crossAxisAlignment: WrapCrossAlignment.end,
                        children: [
                          TextField(
                              decoration: const InputDecoration(
                                  hintText: 'Inserisci testo'),
                              autofocus: true,
                              controller: textController,
                              onSubmitted: ((value) {
                                context.read<TodosCubit>().addTodo(
                                    Todo(text: textController.text, id: id));
                                Navigator.of(context).pop();
                              })),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: ElevatedButton(
                              onPressed: () {
                                context.read<TodosCubit>().addTodo(
                                    Todo(text: textController.text, id: id));
                                Navigator.of(context).pop();
                              },
                              child: const Text('Add'),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                )));

        // print(TodosCubit().state);
      },
    );
  }
}
