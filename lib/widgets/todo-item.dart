import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myapp/cubits/todo-cubit.dart';
import 'package:myapp/models/todo.dart';

class TodoItem extends StatelessWidget {
  const TodoItem({Key? key, required this.todo}) : super(key: key);

  final Todo todo;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
        key: Key('${todo.id}'),
        direction: DismissDirection.horizontal,
        onDismissed: (direction) {
          if (direction == DismissDirection.startToEnd) {
            context.read<TodosCubit>().removeTodo(todo);
          }
        },
        confirmDismiss: (direction) async {
          if (direction == DismissDirection.endToStart) {
            context.read<TodosCubit>().toggleTodoCheck(todo);
          }
          return direction == DismissDirection.startToEnd;
        },
        dragStartBehavior: DragStartBehavior.start,
        movementDuration: const Duration(milliseconds: 100),
        background: Container(
          alignment: Alignment.centerLeft,
          color: Colors.red,
          child: const Padding(
            padding: EdgeInsets.all(15.0),
            child: Icon(
              Icons.delete,
              color: Colors.white,
            ),
          ),
        ),
        secondaryBackground: Container(
          alignment: Alignment.centerRight,
          color: Colors.green,
          child: const Padding(
            padding: EdgeInsets.all(15),
            child: Icon(
              Icons.check,
              color: Colors.white,
            ),
          ),
        ),
        child: Padding(
          // alignment: Alignment.centerLeft,
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: Row(
            children: [
              Text(
                '${todo.text} ${todo.id}',
                style: TextStyle(
                    fontSize: 20,
                    decoration:
                        todo.completed ? TextDecoration.lineThrough : null),
              ),
            ],
          ),
        ));
  }
}
