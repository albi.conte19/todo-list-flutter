import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myapp/models/todo.dart';

class TodosCubit extends Cubit<List<Todo>> {
  TodosCubit() : super([]);
  void addTodo(Todo todoToAdd) => emit([...state, todoToAdd]);
  void removeTodo(Todo todo) {
    List<Todo> stateCopy = state;
    stateCopy.remove(todo);

    return emit([...stateCopy]);
  }

  void toggleTodoCheck(Todo todoToCheck) {
    List<Todo> stateCopy = state;
    int indexToToggle = stateCopy.indexOf(todoToCheck);
    stateCopy[indexToToggle].completed = !stateCopy[indexToToggle].completed;
    return emit([...stateCopy]);
  }
}
