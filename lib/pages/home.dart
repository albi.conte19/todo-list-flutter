import 'dart:math';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myapp/cubits/todo-cubit.dart';
import 'package:myapp/widgets/add-todo-fab.dart';
import 'package:myapp/widgets/todo-item.dart';

import '../models/todo.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Homepage')),
      body: BlocConsumer<TodosCubit, List<Todo>>(
        listener: (context, state) {
          // print('State Updated');
        },
        builder: (context, state) => ListView.builder(
          itemBuilder: ((context, index) => TodoItem(todo: state[index])),
          itemCount: state.length,
        ),
      ),
      floatingActionButton: const AddTodoFab(),
    );
  }
}
