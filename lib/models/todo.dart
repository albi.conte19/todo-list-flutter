class Todo {
  String text;
  bool completed;
  int id;

  Todo({required this.text, this.completed = false, required this.id});
}
